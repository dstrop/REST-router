<?php
declare(strict_types=1);

namespace RestRouter\Responses;

use Nette;
use Nette\Http\IResponse;
use Nette\Http\IRequest;


class JsonResponse extends Nette\Application\Responses\JsonResponse
{

	/** @var int */
	private $code;


	/**
	 * @param  array|\stdClass  payload
	 * @param  int              HTTP response code
	 * @param  string           MIME content type
	 */
	public function __construct($payload, int $code = IResponse::S200_OK, string $contentType = NULL)
    {
		parent::__construct($payload, $contentType);
		
		$this->code = $code;
	}

	/**
	 * Returns HTTP response code.
	 */
	public function getCode(): int
    {
		return $this->code;
	}

	/**
	 * Sends response to output.
	 */
	public function send(IRequest $httpRequest, IResponse $httpResponse): void
    {
		$httpResponse->setCode($this->code);

		parent::send($httpRequest, $httpResponse);
	}
}
