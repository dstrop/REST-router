<?php
declare(strict_types=1);

namespace RestRouter\Presenters;

use Nette;
use Nette\Application;
use Nette\Http;
use RestRouter\Responses\JsonResponse;

abstract class RestPresenter extends Application\UI\Component implements Application\IPresenter
{
	
	const ACTION_KEY = 'action';
	
	/**
	 * @var JsonResponse
	 */
	private $response;
	
	
	/**
	 * @return Application\IResponse
	 */
	public function run(Application\Request $request): Application\IResponse
    {
		try {
			$this->setParent($this->getParent(), $request->getPresenterName());
			
			$parametrs = $request->getParameters();
			$action = $parametrs[self::ACTION_KEY];

			if(!$this->tryCall($action, $parametrs)) {
				throw new BadRequestException('Action not found.');
			}
		} catch (Application\AbortException $e) {
		} catch (\Exception $e) {
			$this->response = new JsonResponse(['error' => $e->getMessage()], $e->getCode());
		}
		
		if($this->response === NULL) {
			$this->response = new JsonResponse([], Http\IResponse::S204_NO_CONTENT);
		}
		
		return $this->response;
	}
	
	
	/**** Copied form Application\UI\Presenter ****/

	/**
	 * Sends JSON data to the output.
	 * @param  mixed $data
	 * @throws Nette\Application\AbortException
	 */
	public function sendJson($data, int $code = Http\IResponse::S200_OK): void
    {
		$this->sendResponse(new JsonResponse($data, $code));
	}

	/**
	 * Sends response and terminates presenter.
	 * @throws Nette\Application\AbortException
	 */
	public function sendResponse(Application\IResponse $response): void
    {
		$this->response = $response;
		$this->terminate();
	}

	/**
	 * Correctly terminates presenter.
	 * @throws Nette\Application\AbortException
	 */
	public function terminate(): void
    {
		throw new Application\AbortException();
	}
}
