<?php
declare(strict_types=1);

namespace RestRouter\Routes;

use Nette;
use Nette\Application\Request;
use Nette\Http\IRequest;
use Nette\Application\Routers\Route;
use RestRouter\Presenters\RestPresenter;

class RestRoute extends Route
{
	
	const DEFAULT_METHODS = [
		IRequest::POST    => 'create',
		IRequest::GET     => 'read',
		IRequest::PUT     => 'update',
		IRequest::DELETE  => 'delete'
	];
	
	/**
	 * @var array Array matching request methods to actions.
	 */
	protected $methods = [];

	/**
	 * @param  string  URL mask, e.g. '<presenter>/<action>/<id \d{1,3}>'
	 * @param  array|string|\Closure  default values or metadata or callback for NetteModule\MicroPresenter
	 * @param  array   Array matching request methods to actions.
	 * @param  int     flags
	 * @throws Nette\InvalidArgumentException
	 */
	public function __construct(string $mask, $metadata = [], array $methods = self::DEFAULT_METHODS, int $flags = 0)
    {
		parent::__construct($mask, $metadata, $flags);

		if (count($methods) === 0) {
			throw new Nette\InvalidArgumentException('No methods have been set.');
		}
		
		$this->methods = $methods;
	}
	
	
	/**
	 * @return Request|NULL
	 */
	public function match(IRequest $httpRequest): ?Request
    {
		if(!isset($this->methods[$httpRequest->getMethod()])) {
			return NULL;
		}
		
		$applicationRequest = parent::match($httpRequest);
		
		if($applicationRequest === NULL) {
			return NULL;
		}
		
		$parametrs = $applicationRequest->getParameters();
		$parametrs[RestPresenter::ACTION_KEY] = $this->methods[$httpRequest->getMethod()];
		$applicationRequest->setParameters($parametrs);

		return $applicationRequest;
	}

	/**
	 * @return string|NULL
	 */
	function constructUrl(Nette\Application\Request $appRequest, Nette\Http\Url $refUrl): ?string
    {
		$parameters = $appRequest->getParameters();
		unset($parameters[RestPresenter::ACTION_KEY]);
		$appRequest->setParameters($parameters);
		
		return parent::constructUrl($appRequest, $refUrl);
	}
}
